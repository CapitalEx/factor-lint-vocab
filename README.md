# lint.vocabs

`lint.vocabs` is a vocabulary for [Factor](https://factorcode.org/) that provides the ability to find unused
imports. It can be used in the listener as simply as:

```factor
"my.cool.vocab" find-unused.
```

## Install

Just add the `lint` folder to one of your Factor directories such as `/work/` or `/extra/`.